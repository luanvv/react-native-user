import React, {Component} from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';

import {
    Router,
    Scene,
    Actions,
    ActionConst,
} from 'react-native-router-flux';

import LoginScreen from './screens/LoginScreen';
import SecondScreen from './screens/SecondScreen';
import SignUpScreen from './screens/SignUpScreen';
import UserListScreen from './screens/UserListScreen';
import UserDetailScreen from './screens/UserDetailScreen';

export default class Main extends Component {
    render(){
        return (
             <Router>
                <Scene key="root">
                    <Scene key="loginScreen"
                        component={LoginScreen}
                        animation='fade'
                        hideNavBar={true}
                        initial={false} />
                    <Scene key="secondScreen"
                        component={SecondScreen}
                        animation='fade'
                        hideNavBar={true} />
                    <Scene key="signUpScreen"
                        component={SignUpScreen}
                        animation='fade'
                        hideNavBar={false}
                        initial={false} />
                    <Scene key="userListScreen"
                        component={UserListScreen}
                        animation='fade'
                        hideNavBar={false}
                        initial={true} />
                    <Scene key="userDetailScreen"
                        component={UserDetailScreen}
                        animation='fade'
                        hideNavBar={false}
                        initial={false} />
                </Scene>
            </Router>
        )
    }
}