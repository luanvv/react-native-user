import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  FlatList,
  Alert,
  ActivityIndicator,
  TouchableOpacity,
} from "react-native";
import Dimensions from 'Dimensions';

import { List, ListItem } from 'react-native-elements';
import {Actions} from 'react-native-router-flux';

export default class UserListScreen extends React.PureComponent {

	constructor(props) {
		super(props);

		this.state = {
			loading: false,
			data: [],
			page: 1,
			seed: 1,
			gender: null,
			error: null,
			refreshing: false,
		};

	}
	componentDidMount() {
		this.makeRemoteRequest();
	}

	makeRemoteRequest = () => {
    const { page, seed , gender} = this.state;
	const url = 'https://randomuser.me/api/?' + (seed > 0 ? `seed=${seed}&` : '') + `gender=${gender}&page=${page}&results=20`;
    this.setState({ loading: true });

    fetch(url)
      .then(res => res.json())
      .then(res => {
        this.setState({
          data: page === 1 ? res.results : [...this.state.data, ...res.results],
          error: res.error || null,
          loading: false,
          refreshing: false
        });
      })
      .catch(error => {
        this.setState({ error, loading: false });
      });
  };

  	handRefresh = () => {
		this.setState(
			{
				 page: 1,
				seed: this.state.seed + 1,
				refreshing: true,
		  },
			() => {
				this.makeRemoteRequest();
			});
	}

	handleLoadMore = () => {
		this.setState(
			{
				page: this.state.page + 1,
			},
			() => {
				this.makeRemoteRequest();
			}
		);
	}

	renderFooter = () => {
		if(!this.state.loading) return null;
		return (
			<View
				style={{
					paddingVertical: 20,
					borderTopWidth: 1,
					borderColor: "#CED0CE",
				}}>
					<ActivityIndicator animating size="large" />
				</View>
		)
	}
	renderSeparator  = () => {
		return (
			<View style={{
				height: 1,
				width: "86%",
				backgroundColor:  '#CED0CE',
				marginLeft: "14%",
			}} />
		);
	}
	render(){
		return (
			<View>
				<View style={styles.header}></View>
				<List style={{borderTopWidth: 0, borderBottomWidth: 0}}>
					<FlatList
						style={styles.listSongs}	
						data={this.state.data}
						keyExtractor={item => item.email}
						ItemSeparatorComponent={this.renderSeparator}
						ListFooterComponent={this.renderFooter}
						onRefresh={this.handRefresh}
						refreshing={this.state.refreshing}
						renderItem={({ item }) => (
							<ListItem
								roundAvatar
								title={`${item.name.first} ${item.name.last}`}
								subtitle={item.email}
								key={item.email}
								avatar={{ uri: item.picture.thumbnail }}
								style={{borderBottomWidth: 0}}
								onPress={()=>{Actions.userDetailScreen({item})}}
							/>
							)}
					/>
					</List>
			</View>
		)
	}
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
	header: {
		height: 65,
	},
	listSongs: {
		width: DEVICE_WIDTH - 40,
		marginLeft: 20,
	}
});