import React, {Component, PropTypes} from 'react';
import Logo from '../components/Logo';
import Wallpaper from '../components/Wallpaper';
import LoginForm from '../forms/LoginForm';
import ButtonSubmit from '../components/ButtonSubmit';
import SignupSection from './SignupSection';

export default class LoginScreen extends Component {
    render(){
        return (
            <Wallpaper>
                <Logo />
                <LoginForm />
                <SignupSection/>
                <ButtonSubmit/>
            </Wallpaper>
        );
    }
}