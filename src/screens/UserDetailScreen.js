import React, {Component} from 'react';
import { Text, View, Image, StyleSheet, TouchableOpacity, } from "react-native";

import Dimensions from 'Dimensions';
import { List, ListItem } from 'react-native-elements';
import Icon from "react-native-vector-icons/Entypo";
import { Avatar } from "react-native-elements";
export default class UserDetailScreen extends Component {

    constructor(props){
        super(props);
    }

    componentWillMount(){
        if(typeof this.props.item === 'undefined'){
             this.props = {
                  item:  {  
                            "gender":"female",
                            "name":{  
                                "title":"ms",
                                "first":"aubrey",
                                "last":"gill"
                            },
                            "location":{  
                                "street":"9487 maple ave",
                                "city":"dorchester",
                                "state":"british columbia",
                                "postcode":78437
                            },
                            "email":"aubrey.gill@example.com",
                            "login":{  
                                "username":"goldenleopard724",
                                "password":"snoopy1",
                                "salt":"jPFYmCcq",
                                "md5":"d174ee2abd9ab3b310c9e9e6f0916a93",
                                "sha1":"f17e78cea0a8eef0eeed1940547c5681d261965b",
                                "sha256":"1ac50d154f7e36b4b74487f8cc4b45b84ca8a8af1b791cfc8bee543dc6bb13d0"
                            },
                            "dob":"1983-10-30 07:44:56",
                            "registered":"2009-10-03 19:51:11",
                            "phone":"100-495-1471",
                            "cell":"740-557-0697",
                            "id":{  
                                "name":"",
                                "value":null
                            },
                            "picture":{  
                                "large":"https://randomuser.me/api/portraits/women/3.jpg",
                                "medium":"https://randomuser.me/api/portraits/med/women/3.jpg",
                                "thumbnail":"https://randomuser.me/api/portraits/thumb/women/3.jpg"
                            },
                            "nat":"CA"
                        }
            }
        }
    }

    renderDetail(item){
        return <View style={styles.detailWrapper}>
            <View style={styles.avatarWrapper}>
              <Avatar xlarge  style={styles.avatar} source={{ uri: item.picture.large }} rounded />
              <View style={styles.name}>
                <Text>{`${item.name.first} ${item.name.last}`}</Text>
              </View>
            </View>

            <View style={styles.seperateBlockStart} />
            <View style={styles.detailBlock}>
              <Icon name="home" size={24} color={iconColor} />
              <View style={styles.innerDetailTextWrapper}>
                <Text style={styles.innerDetailText}>City</Text>
                <Text style={styles.innerDetailText}>
                  {item.location.city}
                </Text>
              </View>
            </View>
            <View style={styles.seperateBetweenBlock} />
            <View style={styles.detailBlock}>
              <Icon name="address" size={24} color={iconColor} />
              <View style={styles.innerDetailTextWrapper}>
                <Text style={styles.innerDetailText}>State</Text>
                <Text style={styles.innerDetailText}>
                  {item.location.state}
                </Text>
              </View>
            </View>
            <View style={styles.seperateBetweenBlock} />
            <View style={styles.detailBlock}>
              <Icon name="phone" size={24} color={iconColor} />
              <View style={styles.innerDetailTextWrapper}>
                <Text style={styles.innerDetailText}>Phone number</Text>
                <Text style={styles.innerDetailText}>{item.phone}</Text>
              </View>
            </View>
            <View style={styles.seperateBlockEnd} />

            <View style={styles.seperateBlockStart} />
            <View style={styles.detailBlock}>
              <Icon name="user" size={24} color={iconColor} />
              <View style={styles.innerDetailTextWrapper}>
                <Text style={styles.innerDetailText}>Username</Text>
                <Text style={styles.innerDetailText}>
                  {item.login.username}
                </Text>
              </View>
            </View>
            <View style={styles.seperateBetweenBlock} />
            <View style={styles.detailBlock}>
              <Icon name="email" size={24} color={iconColor} />
              <View style={styles.innerDetailTextWrapper}>
                <Text style={styles.innerDetailText}>Email</Text>
                <Text style={styles.innerDetailText}>{item.email}</Text>
              </View>
            </View>
            <View style={styles.seperateBlockEnd} />
          </View>;
    }
    render(){
        return (
            <View style={styles.container}>
                <View style={styles.header} />
                {this.renderDetail(this.props.item)}
            </View>
        );
    }
}

const iconColor = "#304FFE";
 const styles = StyleSheet.create({
    header: {
        height: 60,
    },
    container: {
        
    },
    detailWrapper : {
        flex: 1,
        flexDirection: 'column',
    },
    avatarWrapper: {
        alignItems: 'center',
        height: 200,
    },
    avatar: {
        width: 128,
        height: 128,
        marginTop: 20,
    },
    name: {
        marginTop: 15,
    },
    detailBlock: {
       marginTop: 5,
        height: 25,
        flexDirection: 'row',
    },
    avatarSmall: {
        width: 24,
        height: 24,
    },
    innerDetailTextWrapper: {
        justifyContent: 'center',
        marginLeft: 10,
        height: 24,
    },
    innerDetailText: {
        fontSize: 12,
    },
    seperateBlockStart: {
        marginTop: 5,
        marginBottom: 10,
        borderWidth: 0.15,
    },
    seperateBetweenBlock: {
        marginTop: 5,
        marginLeft: 34,
        borderWidth: 0.15,
    },
    seperateBlockEnd: {
        marginTop: 5,
        marginBottom: 10,
        height: 1,
        borderWidth: 0.2,
    }
})