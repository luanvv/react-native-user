import React, {Component, PropTypes} from 'react';
import Dimensions from 'Dimensions';
import {
    StyleSheet,
    View,
    Text,
    Alert,
    TouchableOpacity,
} from 'react-native';
import {Actions} from 'react-native-router-flux';

export default class SignUpSection extends Component {
    constructor(){
        super();
        this._onPress = this._onPress.bind(this);
    }
    _onPress(){
        Actions.signUpScreen();
    }
    render (){
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={this._onPress} activeOpacity={1} style={styles.button}>
                    <Text style={styles.text}>Create Account</Text>
                </TouchableOpacity>
                
                <Text style={styles.text}>Forgot password</Text>
            </View>
        );
    }
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        top: 65,
        width: DEVICE_WIDTH,
        flexDirection: 'row',
    },
    button: {
        zIndex: 100,
    },
    text: {
        color: 'white',
        backgroundColor: 'transparent',
    }
});