import React, {Component} from 'react';

import {
	StyleSheet,
	View,
	Text,
	KeyboardAvoidingView,
	TextInput,
	Animated,
	Alert,
	TouchableOpacity,
} from 'react-native';
import {Actions} from 'react-native-router-flux';
import Dimensions from 'Dimensions';

export default class SignUpScreen extends Component{
	constructor(){
		super();
		this.state = {
			firstName: '',
			lastName: '',
			email: '',
			password: '',
			rePassword:'',
			errorTxts: [],
			validRePasswordMsg: '',
		}
		this._checkMatchPassword = this._checkMatchPassword.bind(this);
		this._checkRequired = this._checkRequired.bind(this);
		this._register = this._register.bind(this);
	}

	_checkMatchPassword(){
		error = (this.state.rePassword !== '' && this.state.password !== this.state.rePassword) ? 'Password is not match' : '';
		errorTxts = this.state.errorTxts;
		errorTxts[4] = error;
		this.setState({
			errorTxts: errorTxts
		});
	}

	_checkRequired(index){
		errorTxts = this.state.errorTxts;
		reError = 'is required';
		switch(index){
			case 0: 
				errorTxts[index] = this.isEmpty(this.state.firstName) ? reError : '';
				break;
			case 1: 
				errorTxts[index] = this.isEmpty(this.state.lastName) ? reError : '';
				break;
			case 2: 
				errorTxts[index] = this.isEmpty(this.state.email) ? reError : '';
				break;
			case 3: 
				errorTxts[index] = this.isEmpty(this.state.password) ? reError : '';
				break;
		}
		this.setState({
			errorTxts: errorTxts,
		})
	}

	isEmpty(value){
		return value.trim() === '';
	}

	_register(){
		Alert.alert(
			'Info',
			'',
			[
				{text: 'OK', onPress: () => Actions.userListScreen()},
				{text: 'Cancle'},
			],
			{canclable: false}
		)
	}

	render(){
		return (
			<View style={styles.container}>
				<Text style={styles.header}>Sign Up</Text>
				<KeyboardAvoidingView>
					<TextInput style={styles.textInput} 
						placeholder='First name'
						returnKeyType='done'
						value={this.state.firstName}
						onChangeText={(firstName) => this.setState({firstName})}
						onEndEditing={() => this._checkRequired(0)}
						/>
					<Text style={styles.errorTxt}>{this.state.errorTxts[0]}</Text>
					<TextInput style={styles.textInput} 
						placeholder='Last name'
						returnKeyType='done'
						value={this.state.lastName}
						onChangeText={(lastName) => this.setState({lastName})}
						onEndEditing={() => this._checkRequired(1)}
						/>
					<Text style={styles.errorTxt}>{this.state.errorTxts[1]}</Text>
					<TextInput style={styles.textInput} 
						placeholder='Email'
						returnKeyType='done'
						keyboardType='email-address'
						onChangeText={(email) => this.setState({email})}
						onEndEditing={() => this._checkRequired(2)}
						/>
					<Text style={styles.errorTxt}>{this.state.errorTxts[2]}</Text>
					<TextInput style={styles.textInput} 
						placeholder='Password'
						returnKeyType='done'
						secureTextEntry={true}
						onChangeText={(password) => this.setState({password})}
						onEndEditing={() => this._checkRequired(3)}
						/>
					<Text style={styles.errorTxt}>{this.state.errorTxts[3]}</Text>
					<TextInput style={styles.textInput} 
						placeholder='Re-Password'
						returnKeyType='done'
						secureTextEntry={true}
						onChangeText={(rePassword) => this.setState({rePassword})}
						onEndEditing={this._checkMatchPassword}
						/>
					<Text style={styles.errorTxt}>{this.state.errorTxts[4]}</Text>
				</KeyboardAvoidingView>
				<TouchableOpacity style={styles.registerBtn} onPress={this._register}>
					<Text style={styles.txt}>Register</Text>
				</TouchableOpacity>
			</View>
		)
	}
}

const DEVICE_WIDTH = Dimensions.get('window').width;
const DEVICE_HEIGHT = Dimensions.get('window').height;

const styles = StyleSheet.create({
	container: {
		top: 65,
		alignItems: 'center',
	},
	header: {
		fontSize: 20,
		fontWeight: 'bold',
		alignItems: 'center',
	},
	textInput: {
		height: 40,
		width: DEVICE_WIDTH - 40,
		borderRadius: 20,
		color: '#000000',
		fontSize: 17,
	},
	errorTxt: {
		color: 'red',
	},
	txt: {
		fontSize: 22,
		color: '#FFFFFF',
		fontWeight: 'bold',
	},
	registerBtn: {
		top: 15,
		width: DEVICE_WIDTH - 40,
		height: 35,
		borderRadius: 20,
		alignItems: 'center',
		justifyContent: 'center',
		backgroundColor: '#000000',
	}
})